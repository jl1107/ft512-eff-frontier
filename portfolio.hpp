#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__

#include "asset.hpp"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>
#include <Eigen/Dense>
using namespace Eigen;



class Portfolio {
public:
  int n; //number of assets
  VectorXd weight; //a vector of weights
  std::vector<Asset> A; //a list of Assets
  double vol; //portfolio volatility
  double ror; //portfolio rate of return
  MatrixXd Corr; //correlation matrix
public:
  Portfolio() {}
  ~Portfolio() {}
  void calculateROR() {
    double sum = 0;
    for (int i = 0; i < n; i++) {
      sum += weight(i) * A[i].avr;
    }
    ror = sum;
  }
  void calculateVol() {
    double sum = 0;
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        sum += weight(i) * weight(j) * A[i].sigma * A[j].sigma * Corr(i,j);
      }
    }
    vol = std::sqrt(sum);
  }
};

#endif
