#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include <iomanip>
#include <unistd.h>

#include "asset.hpp"
#include "portfolio.hpp"
#include "parse.hpp"



int main(int argc, char ** argv) {
  //Check whether "-r" is passed in
  int opt;
  bool r = false;
  while ((opt = getopt(argc, argv, "r")) != -1) {
    switch (opt) {
      case 'r':
        r = true;
        break;
      case '?':
        std::cerr << "Option format is wrong.\n";
        return EXIT_FAILURE;
    }
  }

  //Read universe file and correlation matrix
  int num = 0; //number of assets
  std::vector<Asset> A;
  MatrixXd Corr;
  if ((r == true) && (argv[1][0] == '-') && (argc == 4)) {
    A = readUniverse(argv[2], num);
    Corr = readCorrMatrix(argv[3], num);
  }
  else if ((r == true) && (argv[2][0] == '-') && (argc == 4)) {
    A = readUniverse(argv[1], num);
    Corr = readCorrMatrix(argv[3], num);
  }
  else if ((r == true) && (argv[3][0] == '-') && (argc == 4)) {
    A = readUniverse(argv[1], num);
    Corr = readCorrMatrix(argv[2], num);
  }
  else if ((r == false) && (argc == 3)) {
    A = readUniverse(argv[1], num);
    Corr = readCorrMatrix(argv[2], num);
  }
  else {
    std::cerr << "The number of inputs is wrong.\n";
    return EXIT_FAILURE;
  }

  Portfolio P;
  P.n = num;
  P.A = A;
  P.Corr = Corr;

  //Unrestricted case: return level between 1% and 26% in 1% increments
  /*
  ROR,volatility
  1.0%,0.78%
  */
  std::cout << "ROR,volatility" << std::endl;
  std::cout.setf(std::ios::fixed);
  if (r == false) {
    for (double l = 0.01; l < 0.265; l += 0.01) {
      std::cout << std::fixed << std::setprecision(1) << l * 100 << "%,";
      std::cout << std::fixed << std::setprecision(2) << unrestricted(P, l) * 100 << "%" << std::endl;
    }
  }

  //Restricted case: return level between 1% and 26% in 1% increments

  if (r == true) {
    for (double l = 0.01; l < 0.265; l += 0.01) {
      std::cout << std::fixed << std::setprecision(1) << l * 100 << "%,";
      std::cout << std::fixed << std::setprecision(2) << restricted(P, l) * 100 << "%" << std::endl;
    }
  }

  std::cout.unsetf(std::ios::fixed);
  return EXIT_SUCCESS;

}
