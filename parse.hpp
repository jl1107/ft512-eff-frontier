#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "asset.hpp"
#include "portfolio.hpp"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>
#include <Eigen/Dense>
using namespace Eigen;



//Split string with delimiter(input string, output vector, delimiter)
void splitString(std::string& s, std::vector<std::string>& v, std::string& c);

//Read data from universe.csv
std::vector<Asset> readUniverse(char* filename, int &num);

//Read correlation matrix from correlation.csv
MatrixXd readCorrMatrix(char* filename, int num);

//Unrestricted case
double unrestricted(Portfolio &P, double ret);

//Restricted case
double restricted(Portfolio &P, double ret);

#endif
