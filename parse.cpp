#include "asset.hpp"
#include "portfolio.hpp"
#include "parse.hpp"

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <algorithm>
#include <Eigen/Dense>
using namespace Eigen;



//Split string with delimiter(input string, output vector, delimiter)
void splitString(std::string& s, std::vector<std::string>& v, std::string& c) {
  std::string::size_type pos1, pos2;
  pos2 = s.find(c);
  pos1 = 0;
  while (std::string::npos != pos2) {
    v.push_back(s.substr(pos1, pos2 - pos1));
    pos1 = pos2 + c.size();
    pos2 = s.find(c, pos1);
  }
  //Data after the last delimiter
  if (pos1 != s.length()) {
    v.push_back(s.substr(pos1));
  }
}





//Read data from universe.csv
/*
universe.csv:
Intl Equity,0.265095,0.312690
*/
std::vector<Asset> readUniverse(char* filename, int &num) {
  std::ifstream ifs(filename);
  if (!ifs.is_open()) {
    std::cerr << "Could not open the universe file.\n";
    exit(EXIT_FAILURE);
  }

  std::vector<Asset> A;
  std::string line;
  std::string c = ",";

  while (std::getline(ifs, line)) {
    std::vector<std::string> t;
    splitString(line, t, c);
    if (t.size() != 3) {
      std::cerr << "The universe file has the wrong format.\n";
      exit(EXIT_FAILURE);
    }

    Asset temp;
    temp.name = t[0];
    if ((atof(t[1].c_str()) == 0.0) || (atof(t[2].c_str()) == 0.0)) {
      std::cerr << "The universe file has non-numeric data.\n";
      ifs.close();
      exit(EXIT_FAILURE);
    }

    temp.avr = atof(t[1].c_str());
    temp.sigma = atof(t[2].c_str());
    num++;
    A.push_back(temp);
  }

  if (num == 0) {
    std::cerr << "The universe file is empty.\n";
    ifs.close();
    exit(EXIT_FAILURE);
  }

  return A;
}





//Read correlation matrix from correlation.csv
/*
correlation.csv:
1.000000,0.317725,-0.017881,-0.296454,-0.193029,0.505183,-0.163329
*/
MatrixXd readCorrMatrix(char* filename, int num) {
  std::ifstream ifs(filename);
  if (!ifs.is_open()) {
    std::cerr << "Could not open the correlation file.\n";
    exit(EXIT_FAILURE);
  }

  MatrixXd Corr(num, num);
  std::string line;
  std::string c = ",";

  int i = 0;
  while (std::getline(ifs, line)) {
    if (i == num) {
      std::cerr << "Dimension of correlation matrix is greater than the number of assets.\n";
      ifs.close();
      exit(EXIT_FAILURE);
    }

    std::vector<std::string> t;
    splitString(line, t, c);
    if (t.size() != (size_t)num) {
      std::cerr << "Dimension of correlation matrix is not the same as the number of assets.\n";
      ifs.close();
      exit(EXIT_FAILURE);
    }

    for (int j = 0; j < num; j++) {
      if (atof(t[j].c_str()) == 0.0) {
        std::cerr << "Correlation matrix has non-numeric data.\n";
        ifs.close();
        exit(EXIT_FAILURE);
      }
      Corr(i, j) = atof(t[j].c_str());
    }
    i++;
  }

  if (i == 0) {
    std::cerr << "The correlation file is empty.\n";
    ifs.close();
    exit(EXIT_FAILURE);
  }

  for (int i = 0; i < num; i++) {
    for (int j = 0; j <= i; j++) {
      if ((fabs(Corr(i, j) - Corr(j, i)) > 0.0001) || (fabs(Corr(i, j)) > 1.0001)) {
        std::cerr << "Correlation matrix has wrong mathematical format.\n";
        ifs.close();
        exit(EXIT_FAILURE);
      }
    }
    if (fabs(Corr(i, i) - 1) > 0.0001) {
      std::cerr << "The values of the diagonal of correlation matrix must be 1.\n";
      ifs.close();
      exit(EXIT_FAILURE);
    }
  }

  return Corr;
}





//Unrestricted case: optimal portfolio volatility given expected portfolio rate of return
double unrestricted(Portfolio &P, double ret) {
  int n = P.n;
  //Initialize matrix A(2*n): [1, r]^T
  MatrixXd A1 = MatrixXd::Ones(1, n);
  MatrixXd A2(1, n);
  for (int i = 0; i < n; i++) {
    A2(i) = P.A[i].avr;
  }
  MatrixXd A(2, n);
  A << A1, A2;

  //Initialize matrix B((n+2)*1): [0, b]^T -> b(2*1): [1, r_p]^T
  MatrixXd b1 = MatrixXd::Zero(n, 1);
  MatrixXd b2(2, 1);
  b2 << 1, ret;
  MatrixXd B(n+2, 1);
  B << b1, b2;

  //Initialize covariance matrix(n*n)
  MatrixXd Cov(n, n);
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      Cov(i, j) = P.A[i].sigma * P.A[j].sigma * P.Corr(i, j);
    }
  }

  //Initialize matrix M((n+2)*(n+2)): [(Cov, A^T), (A, O)]
  MatrixXd O = MatrixXd::Zero(2, 2);
  MatrixXd M(n+2, n+2);
  M << Cov, A.transpose(), A, O;

  //Solve the equation: MX = B -> X((n+2)*1)
  VectorXd X(n+2, 1);
  X = M.fullPivHouseholderQr().solve(B);

  P.weight = X.head(n);
  P.calculateVol();
  return P.vol;
}





//Restricted case: optimal portfolio volatility given expected portfolio rate of return (no short sale)
double restricted(Portfolio &P, double ret) {
  int n = P.n;
  //Initialize matrix A(2*n): [1, r]^T
  MatrixXd A1 = MatrixXd::Ones(1, n);
  MatrixXd A2(1, n);
  for (int i = 0; i < n; i++) {
    A2(i) = P.A[i].avr;
  }
  MatrixXd A(2, n);
  A << A1, A2;

  //Initialize matrix B((n+2)*1): [0, b]^T -> b(2*1): [1, r_p]^T
  MatrixXd b1 = MatrixXd::Zero(n, 1);
  MatrixXd b2(2, 1);
  b2 << 1, ret;
  MatrixXd B(n+2, 1);
  B << b1, b2;

  //Initialize covariance matrix(n*n)
  MatrixXd Cov(n, n);
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      Cov(i, j) = P.A[i].sigma * P.A[j].sigma * P.Corr(i, j);
    }
  }

  //Initialize matrix M((n+2)*(n+2)): [(Cov, A^T), (A, O)]
  MatrixXd O = MatrixXd::Zero(2, 2);
  MatrixXd M(n+2, n+2);
  M << Cov, A.transpose(), A, O;

  //Solve the equation: MX = B -> X((n+2)*1) to find the unrestricted weights first
  VectorXd X(n+2, 1);
  X = M.fullPivHouseholderQr().solve(B);

  /*Add an equality constraint that weight be exactly zero
  Exactly solve the new system
  Repeat these steps as long as at least one weight is negative.
  */
  MatrixXd AA = A;
  MatrixXd BB = B;
  VectorXd XX = X;

  for (int i = 0; ; i++) {
    int flag = 1;
    MatrixXd C; //constraint matrix
    int k = 0;
    //Check for any weights that are negative
    for (int j = 0; j < n; j++) {
      if (XX(j) < 0) {
        MatrixXd T = MatrixXd::Zero(1, n); //temp matrix
        T(0, j) = 1; // T = [0 ... 0 1 0 ... 0]--jth element is 1
        MatrixXd temp = C;
        C.resize(k+1, n);
        if (k == 0) {
          C << T;
        }
        else {
          C << temp, T; //add constraints that weights be exactly zero
        }
        flag = 0;
        k++;
      }
    }

    //Stop if there are no new negative weights
    if (flag == 1) {
      break;
    }
    MatrixXd T; //temp matrix
    T = AA;
    //Add rows to matrix A for new constraints
    AA.resize(T.rows() + C.rows(), n);
    AA << T, C;

    //Add rows to vector b for new constraints
    T = BB;
    BB.resize(T.rows() + C.rows(), 1);
    BB << T, MatrixXd::Zero(C.rows(), 1);

    //Initialize new matrix M
    MatrixXd OO = MatrixXd::Zero(AA.rows(), AA.rows());
    MatrixXd MM(Cov.rows() + AA.rows(), Cov.rows() + AA.rows());
    MM << Cov, AA.transpose(), AA, OO;

    //Solve the new equation
    XX = MM.fullPivHouseholderQr().solve(BB);
  }

  P.weight = XX.head(n);
  P.calculateVol();
  return P.vol;
}
